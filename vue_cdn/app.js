
const app = Vue.createApp({
    name: "App",
    data(){
        return{
            todos: [
                {
                    todo: "Vue js",
                    dateCreated: "September 28, 1996",
                    deadline: "",
                    dateFinished: "",
                    isFinished: true
                },{
                    todo: "Taiwind",
                    dateCreated: "September 28, 1996",
                    deadline: "",
                    dateFinished: "",
                    isFinished: true
                },{
                    todo: "Nux",
                    dateCreated: "September 28, 1996",
                    deadline: "",
                    dateFinished: "",
                    isFinished: false
                },
                
            ],
            todoTitle: "Todo for today",
            age: 0
        }
    },
    methods: {
        todoContent(e,data){
            // passed the event object in HTML on click
        console.log("running AddTodo",e.type,data, this.age, this.todoTitle)
        }
    },
    computed: {
        unFinishedTodo(){
            return this.todos.filter((todo)=> !todo.isFinished)
        },
        finishedTodo(){
            return this.todos.filter((todo)=> todo.isFinished)
        }
    }
})

app.mount("#app")
